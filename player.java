
public class Player {
	private char name;
	private int win;
	private int lose;
	private int draw;

	public int getWin() {
		return win;
	}

	public void win() {
		this.win = win++;
	}

	public int getLose() {
		return lose;
	}

	public void lose() {
		this.lose = lose++;
	}

	public int getDraw() {
		return draw;
	}

	public void draw() {
		this.draw = draw++;
	}

	public void setName(char name) {
		this.name = name;
	}

	public Player(char name) {
		super();
		this.name = name;
	}

	public char getName() {
		return name;
	}

	@Override
	public String toString() {
		return "Player [name=" + name + "]";
	}
	

}
